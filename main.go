package main

import (
	"scheduler-extender/core"
	"scheduler-extender/router"
)

func main() {

	// init
	core.Init()
	router.Init()

	// run
	core.Run()
}
