package impl

import "k8s.io/kube-scheduler/extender/v1"

type FilterServiceImpl struct {
}

func (i *FilterServiceImpl) Filter(args v1.ExtenderArgs) v1.ExtenderFilterResult {
	// 简单实现，理论上需要判断node的各种状态
	nodeNames := []string{"ceph1", "ceph2"}
	return v1.ExtenderFilterResult{NodeNames: &nodeNames}
}
