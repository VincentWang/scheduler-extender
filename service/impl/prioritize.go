package impl

import v1 "k8s.io/kube-scheduler/extender/v1"

type PrioritizeServiceImpl struct {
}

func (i *PrioritizeServiceImpl) Prioritize(args v1.ExtenderArgs) v1.HostPriorityList {
	list := make(v1.HostPriorityList, 2)
	p1 := v1.HostPriority{Host: "ceph1", Score: 100}
	p2 := v1.HostPriority{Host: "ceph2", Score: 50}
	list = append(list, p1)
	list = append(list, p2)
	return list
}
