package service

import (
	v1 "k8s.io/kube-scheduler/extender/v1"
	"scheduler-extender/service/impl"
)

var PrioritizeServiceInstance PrioritizeService = new(impl.PrioritizeServiceImpl)

type PrioritizeService interface {
	Prioritize(args v1.ExtenderArgs) v1.HostPriorityList
}
