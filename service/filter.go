package service

import (
	"k8s.io/kube-scheduler/extender/v1"
	"scheduler-extender/service/impl"
)

var FilterServiceInstance FilterService = new(impl.FilterServiceImpl)

type FilterService interface {
	Filter(args v1.ExtenderArgs) v1.ExtenderFilterResult
}
