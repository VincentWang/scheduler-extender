package controller

import (
	"github.com/gin-gonic/gin"
	v1 "k8s.io/kube-scheduler/extender/v1"
	"scheduler-extender/service"
)

var prioritizeService = service.PrioritizeServiceInstance

type PrioritizeController struct {
}

func (i PrioritizeController) Prioritize(ctx *gin.Context) {
	var args v1.ExtenderArgs
	ctx.BindJSON(&args)
	ctx.JSON(200, prioritizeService.Prioritize(args))
}
