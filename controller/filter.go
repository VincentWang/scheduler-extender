package controller

import (
	"github.com/gin-gonic/gin"
	v1 "k8s.io/kube-scheduler/extender/v1"
	"scheduler-extender/service"
)

var filterService = service.FilterServiceInstance

type FilterController struct {
}

func (i FilterController) Filter(ctx *gin.Context) {
	var args v1.ExtenderArgs
	err := ctx.BindJSON(&args)
	if err != nil {
		ctx.JSON(200, v1.ExtenderFilterResult{Error: err.Error()})
	}
	ctx.JSON(200, filterService.Filter(args))
}
