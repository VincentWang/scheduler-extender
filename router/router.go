package router

import (
	"scheduler-extender/controller"
	"scheduler-extender/core"
)

func Init() {
	var router = core.Engine
	var basePath = core.Config.Server.Prefix

	var filterController controller.FilterController
	router.POST(basePath+"/filter", filterController.Filter)

	var prioritizeController controller.PrioritizeController
	router.POST(basePath+"/prioritize", prioritizeController.Prioritize)
}
