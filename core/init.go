package core

import (
	"github.com/gin-gonic/gin"
)

var Config AppConfig
var Engine *gin.Engine
var ConfigPath string

func Init() {
	initConfig()
	initGin()
}

func Run() {
	runGin()
}
