package core

import (
	"github.com/gin-gonic/gin"
	"log"
	"strconv"
)

func initGin() {
	Engine = gin.Default()

	// set mode for prd environment
	if Config.Env == PRD {
		gin.SetMode(gin.ReleaseMode)
	}
}

func runGin() {
	// set server port
	var port = 8080
	p := Config.Server.Port
	if p > 0 && p < 65535 {
		port = p
	}

	// run server
	err := Engine.Run(":" + strconv.Itoa(port))
	if err != nil {
		log.Fatal("Start server fail.", err.Error())
	}
}
