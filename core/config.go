package core

import (
	"github.com/BurntSushi/toml"
	"log"
	"os"
	"strings"
)

const (
	DEV = "dev"
	PRE = "pre"
	PRD = "prd"
)

type AppConfig struct {
	Server Server `toml:"server"`
	Env    string
}

type Server struct {
	Port   int    `toml:"port"`
	Prefix string `toml:"prefix"`
}

func initConfig() {
	var config AppConfig

	// build config file path
	env := os.Getenv("RESOURCES_MANAGER_ENV")
	path := getCurrentPath() + "/conf/"
	ConfigPath = path

	// decode from config file
	if _, err := toml.DecodeFile(path+"application.toml", &config); err != nil {
		log.Fatal("Get config file fail.", err.Error())
	}

	// set config object
	config.Env = env
	Config = config
}

func getCurrentPath() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	return strings.Replace(dir, "\\", "/", -1)
}
